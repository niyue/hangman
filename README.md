# hangman
A client program to play the [hangman](https://en.wikipedia.org/wiki/Hangman_(game)) [game](https://github.com/joycehan/strikingly-interview-test-instructions).

# Run the CLI during development
```
bundle exec bin/hangman -p ${your_player_id} -n ${number_of_rounds_to_play}
```

# Turn on debug level logging during development
```
bundle exec bin/hangman -p ${your_player_id} --debug
```

# Run single test
```
rake test TEST=test/word_frequency_gaming_policy_test.rb TESTOPTS="--name=test_default_vocabulary -v"
```

# Auto re-run tests
* Install [nodemon](https://github.com/remy/nodemon)
* Install [terminal-notifier](https://github.com/julienXX/terminal-notifier)
* Run the following command
```
nodemon --exec "rake test TEST=test/word_frequency_gaming_policy_test.rb | tee /dev/tty | terminal-notifier -sound default"
```

# Run multiple client processes to collect vocabulary
* Install [foreman](https://github.com/ddollar/foreman)
* Start multiple clients, the following command will start 5 instances of clients
```
foreman start -c client=5
```

# data source
The English words used by this program is from two data sources:

- [Peter Norvig's compilation](http://norvig.com/ngrams/) of the [1/3 million most frequent English words](http://norvig.com/ngrams/count_1w.txt)
- [https://github.com/dwyl/english-words](https://github.com/dwyl/english-words)

# Score Journal
### 2017-07-25 sum up

- **Highest score:** 1389
- **Longest winning combo:** 80
- **Average game length:** 25 minutes
- **Total vocabulary:** 390k after deduplication, 30k collected from server
- **Total games played:** Around 460 games by multiple client programs, no exact number is recorded and it is just a rough estimation

It is difficult to get higher score because the current vocabulary is too large and it is difficult to find correct word quickly. For example, for pattern "*ER", it matches 19 words in the vocabulary, which has only 1/19 chances to get the right letter.

Some more solutions unverified yet:

- Collect some un-resolved patterns and use them as part of the vocabulary. For example, many word patterns are half-veiled like "*xyz" and they are currently not added to vocabulary, however, they provide certain information to server's vocabulary too and could be used.
- Some randomness may help to enlarge the vocabulary more quickly. The current guessing approach is deterministic, and if the vocabulary is fixed, and if a word cannot be guessed in 10 times, it cannot be guessed if it appears next time. 
- Give more weight to the words collected from server. Currently extended words collected from server are treated the same as the default vocabulary, however, since they are guaranteed in server's vocabulary, for better score, more weight could be given to these words.
- Collect more words from server should help. However, I am not very certain about this because it seems when 20k words collected, the program can achieve >=1340 score with 25% possibilities, but with 28k words collected, it only achieved >=1300 score with 7% possibilities for some reason, probably I was too lucky initially or server is using multiple rotated vocabularies.

### 2017-07-25 06:30 11th try, use a smaller but more similar vocabulary with server

According to the words collected from server, there are around roughly 90,000 words if they are evenly randomly chosen by the server. Use the first most frequent 100,000 words in Peter Norvig's ngram list with 30,000 extended words collected from server. 

It turned out not to be useful because of the vocabulary I used, in paticular much worse for long words. The most frequent 100,000 has no intersection with the 30,000 words collected from server at all. The score went down a lot to around 900.

```
{"totalWordCount"=>80, "correctWordCount"=>65, 
"totalWrongGuessCount"=>429, "score"=>871}
```

```
{"totalWordCount"=>80, "correctWordCount"=>65, 
"totalWrongGuessCount"=>377, "score"=>923}
```

### 2017-07-24 05:35 10th try, with more words collected from server
After running 11 clients for 10 hours, 20k words are collected from server. So we have a vocabulary more close to the vocabulary the server is using.

The 20k vocabulary collected from server obviously helps. 
Within the first 11 runs of the program (finished in 20 minutes by 11 clients), I got:

- 3 scores that are over 1340
- and one play score 1355 is higher than the highest score running in the last 10 hours
- and one play managed to guess all the 80 words again while there was only 1 such archievement in the last 10 hours with 12k vocabulary

In the second round of 11 runs of the program, **score 1389** was achieved.

```
"totalWordCount"=>80, "correctWordCount"=>78, 
"totalWrongGuessCount"=>218, "score"=>1342}
```

```
{"totalWordCount"=>80, "correctWordCount"=>79, 
"totalWrongGuessCount"=>225, "score"=>1355}
```

```
{"totalWordCount"=>80, "correctWordCount"=>80, 
"totalWrongGuessCount"=>253, "score"=>1347}
```
##### UPDATE 2017-07-25 06:10 23k extended words from server running 6 hours
The result is not as good as I expected. 

Since the first 11 runs got the highest score 1389 and 3/11 got scores > 1340, I expected to get higher scores after running it 6 hours with 12 concurrent clients. **The same program got score distribution like this in 6 hours:**

- [1300, 1400], 11 times
- [1200, 1300], 104 times
- [1100, 1200], 49 times
- [1000, 1100], 4 times
- min score: 1031
- max score: 1380 (lower than 1389 I got in 20 minutes in the morning)
- average game length: 25 minutes, primarily because the server is too slow and took 1.3 seconds to respond to each request

### 2017-07-23 18:10 9th try, only submitting the score when score is higher and launch multiple clients to play the game simultaneously

Keep the score loclally in a file, and only submit the score when it is higher than exiting highest score.

The vocabulary collected from server is about 12k, after running 11 clients concurrently and keep playing the game for 10 hours, the highest score is 1351.

```
18:36:08 1271 => 19:44:58 1332 => 01:56:46 1351
```
This is the first time the program managed to guess all the 80 words, with some luck of course.

```
{"totalWordCount"=>80, "correctWordCount"=>80, 
"totalWrongGuessCount"=>249, "score"=>1351}
```

### 2017-07-23 16:55 8th try, combine two vocabularies together
Combine the 330k words vocabulary with the 479k words vocabulary, together with 5k extended words collected from server. Use the deduplicated vocabulary (610k words) for guessing.
The score turns out to be worse instead of better, probably because the 330k vocabulary is not close to the vocabulary in server side.

```
"totalWordCount"=>80, "correctWordCount"=>73, 
"totalWrongGuessCount"=>274, "score"=>1186}
```
```
{"totalWordCount"=>80, "correctWordCount"=>73, 
"totalWrongGuessCount"=>248, "score"=>1212}
```
```
{"totalWordCount"=>80, "correctWordCount"=>71, 
"totalWrongGuessCount"=>284, "score"=>1136}
```


### 2017-07-23 15:30 7th try, start to collect vocabulary from server and use it as extended vocabulary when guessing

Use some local files to keep the words collected from server and multiple concurrent client programs to collect because it is too slow for single client to play a game. 

With 4k extended words, it is almost the same as the previous try.

```
{"totalWordCount"=>80, "correctWordCount"=>72, 
"totalWrongGuessCount"=>290, "score"=>1150}
```
```
{"totalWordCount"=>80, "correctWordCount"=>74, 
"totalWrongGuessCount"=>251, "score"=>1229}
```
```
{"totalWordCount"=>80, "correctWordCount"=>75, 
"totalWrongGuessCount"=>294, "score"=>1206}
```
```
{"totalWordCount"=>80, "correctWordCount"=>75, 
"totalWrongGuessCount"=>249, "score"=>1251}
```

### 2017-07-23 14:20 6th try, switch to use a bigger vocabulary (330k => 479k)
Switch from [Peter Norvig's compilation](http://norvig.com/ngrams/) of the [1/3 million most frequent English words](http://norvig.com/ngrams/count_1w.txt) to [this vocabulary](https://github.com/dwyl/english-words).
It is still difficult to get the short words correct, but it is very likely to get the long words (>=6) correct, and it is very often to see 60 correct guesses in a row. 

There are usually 3~7 words guessed incorrectly out of 80 words,

```
{"totalWordCount"=>80, "correctWordCount"=>73, 
"totalWrongGuessCount"=>277, "score"=>1183}
```

```
{"totalWordCount"=>80, "correctWordCount"=>72, 
"totalWrongGuessCount"=>266, "score"=>1174}
```

```
{"totalWordCount"=>80, "correctWordCount"=>75, 
"totalWrongGuessCount"=>261, "score"=>1239}
```

```
{"totalWordCount"=>80, "correctWordCount"=>79, 
"totalWrongGuessCount"=>252, "score"=>1328}
```

```
{"totalWordCount"=>80, "correctWordCount"=>77, 
"totalWrongGuessCount"=>252, "score"=>1288}
```

### 2017-07-22 23:50 5th try, improve the word frequency gaming policy
The gaming policy will not only use the word pattern, but also using the incorrect letters to match words, so that those words with incorrect letters will not be matched even if they match the current word pattern, and new letter frequency will be dynamically calculated using the matched words.

It should calculate the most likely letter with more accuracy if the vocabulary is similar with server, but it turns out not to be very useful with current vocabulary.

```
{"totalWordCount"=>80, "correctWordCount"=>68, 
"totalWrongGuessCount"=>319, "score"=>1041}
```
```
{"totalWordCount"=>80, "correctWordCount"=>67, 
"totalWrongGuessCount"=>336, "score"=>1004}
```
```
{"totalWordCount"=>80, "correctWordCount"=>76, 
"totalWrongGuessCount"=>294, "score"=>1226}
```
```
{"totalWordCount"=>80, "correctWordCount"=>68, 
"totalWrongGuessCount"=>321, "score"=>1039}}
```
```
{"totalWordCount"=>80, "correctWordCount"=>65, 
"totalWrongGuessCount"=>345, "score"=>955}
```
```
{"totalWordCount"=>80, "correctWordCount"=>69, 
"totalWrongGuessCount"=>363, "score"=>1017}
```
```
{"totalWordCount"=>80, "correctWordCount"=>64, 
"totalWrongGuessCount"=>328, "score"=>952}
```
### 2017-07-22 14:38 4th try, improve the word frequency gaming policy
When two letters are calculated with equal frequency, the policy will use the letter frequency of all English words instead of undetermined order.
After testing, this improvement works not very well for short words in the server (length<=6) while works pretty well for long words.

```
{"totalWordCount"=>80, "correctWordCount"=>68, 
"totalWrongGuessCount"=>325, "score"=>1035}
```

```
{"totalWordCount"=>80, "correctWordCount"=>67, 
"totalWrongGuessCount"=>295, "score"=>1045}
```

```
{"totalWordCount"=>80, "correctWordCount"=>72, 
"totalWrongGuessCount"=>309, "score"=>1131}
```
### 2017-07-22 13:05 3rd try, introduce a new gaming policy using fixed order of letters
Simply return a fixed order of letters according to the letter frequency of all English words, [https://en.wikipedia.org/wiki/Letter_frequency](https://en.wikipedia.org/wiki/Letter_frequency)

```
{"totalWordCount"=>80, "correctWordCount"=>35, 
"totalWrongGuessCount"=>684, "score"=>16}
```
### 2017-07-22 08:55 2nd try, with larger vocabulary

```
{"totalWordCount"=>80, "correctWordCount"=>63, 
"totalWrongGuessCount"=>336, "score"=>924}
```

```
{"totalWordCount"=>80, "correctWordCount"=>62, 
"totalWrongGuessCount"=>362, "score"=>878}
```
### 2017-07-22 08:01 1st try , initial working version
```
{"totalWordCount"=>80, "correctWordCount"=>63, 
"totalWrongGuessCount"=>371, "score"=>889}
```
