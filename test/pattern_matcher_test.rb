require 'corpus/pattern_matcher'
require 'minitest/autorun'

class PatternMatcherTest < Minitest::Test

  def test_match_single_word_with_wildcard_pattern
  	matcher = PatternMatcher.new(['hello'])
  	words = matcher.matched_words('*****')
  	assert_equal ['hello'], words
  end

  def test_match_two_words_with_wildcard_pattern
  	matcher = PatternMatcher.new(['hello', 'world'])
  	words = matcher.matched_words('*****')
  	assert_equal ['hello', 'world'], words
  end

  def test_match_two_words_with_single_letter_pattern
  	matcher = PatternMatcher.new(['hello', 'world'])
  	words = matcher.matched_words('**l**')
  	assert_equal ['hello'], words
  end

  def test_match_two_words_with_last_letter_matched_pattern
    matcher = PatternMatcher.new(['hello', 'world'])
    words = matcher.matched_words('****o')
    assert_equal ['hello'], words
  end

  def test_match_words_with_different_lengths_with_single_letter_pattern
  	matcher = PatternMatcher.new(['hello', 'howdy', 'world', 'hi'])
  	words = matcher.matched_words('h****')
  	assert_equal ['hello', 'howdy'], words
  end

  def test_match_words_with_different_lengths_with_multiple_letters_pattern
  	matcher = PatternMatcher.new(['hello', 'howdy', 'world', 
  								                'heylo', 'where', 'hi'])
  	words = matcher.matched_words('h**l*')
  	assert_equal ['hello', 'heylo'], words
  end

  def test_not_matching_pattern
    matcher = PatternMatcher.new(['hello', 'howdy', 'world', 
                                  'heylo', 'where', 'hi'])
    words = matcher.matched_words('h**l****')
    assert_equal [], words
  end

  def test_match_two_words_with_not_contained_letters
    matcher = PatternMatcher.new(['hello', 'world'])
    words = matcher.matched_words('*****', ['h'].to_set)
    assert_equal ['world'], words
  end

  def test_match_two_words_with_multiple_not_contained_letters
    matcher = PatternMatcher.new(['hello', 'world'])
    words = matcher.matched_words('*****', ['h', 'd'].to_set)
    assert_equal [], words
  end
end
