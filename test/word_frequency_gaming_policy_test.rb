require 'hangman/word_frequency_gaming_policy'
require 'minitest/autorun'

class WordFrequencyGamingPolicyTest < Minitest::Test

  def test_single_word_with_wildcard_pattern_should_return_most_frequent_letter
    policy = WordFrequencyGamingPolicy.new(words: ['hello'])
    letters = policy.run('*****').to_a
    assert_equal 'l', letters[0]
  end

  def test_two_words_with_wildcard_pattern
    policy = WordFrequencyGamingPolicy.new(words: ['hello', 'world'])
    letters = policy.run('*****').to_a
    assert_equal 'l', letters[0]
    assert_equal 'o', letters[1]
  end

  def test_two_words_with_single_letter_pattern
    policy = WordFrequencyGamingPolicy.new(words: ['hello', 'world'])
    letters = policy.run('****o').to_a
    # 'o' should be excluded
    assert_equal 25, letters.size
    assert_equal 'l', letters[0]
    assert ['h', 'e'].include? letters[1]
    assert ['h', 'e'].include? letters[2]
  end

  def test_default_vocabulary
    policy = WordFrequencyGamingPolicy.new
    letters = policy.run('***').to_a
    assert_equal 'a', letters[0]
  end

  def test_default_limit
    policy = WordFrequencyGamingPolicy.new(limit: 200)
    assert policy
  end

  def test_two_words_with_some_not_contained_letters
    policy = WordFrequencyGamingPolicy.new(words: ['hello', 'world', 'heyll'])
    letters = policy.run('***l*', ['w'].to_set).to_a
    assert_equal 25, letters.size
    assert_equal 'e', letters[0]
    assert_equal 'h', letters[1]
    assert ['o', 'y'].include? letters[2]
    assert ['o', 'y'].include? letters[3]
  end

  def test_load_extended_words
    policy = WordFrequencyGamingPolicy.new(use_extended_words: true)
    assert policy
  end
end
