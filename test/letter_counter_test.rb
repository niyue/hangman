require 'corpus/letter_counter'
require 'corpus/corpus_reader'
require 'minitest/autorun'

class LetterCounterTest < Minitest::Test

  def setup
  end

  def teardown
  end

  def test_count_single_word
  	counter = LetterCounter.new(['hello']).count
  	assert_equal 26, counter.size
  	assert_equal 1, counter['h'.ord - 'a'.ord]
  	assert_equal 1, counter['e'.ord - 'a'.ord]
  	assert_equal 2, counter['l'.ord - 'a'.ord]
  	assert_equal 1, counter['o'.ord - 'a'.ord]
  	assert_equal 0, counter['x'.ord - 'a'.ord]
  end

  def test_count_two_words
  	counter = LetterCounter.new(['hello', 'world']).count
  	assert_equal 26, counter.size
  	assert_equal 1, counter['h'.ord - 'a'.ord]
  	assert_equal 1, counter['d'.ord - 'a'.ord]
  	assert_equal 1, counter['e'.ord - 'a'.ord]
  	assert_equal 3, counter['l'.ord - 'a'.ord]
  	assert_equal 2, counter['o'.ord - 'a'.ord]
  	assert_equal 1, counter['r'.ord - 'a'.ord]
  end

  def test_ordered_letters_for_single_word
  	letters = LetterCounter.new(['hello']).counting_ordered_letters.to_a
  	assert_equal 26, letters.size
  	assert_equal 'l', letters[0]
  	assert ['h', 'e', 'o'].include? letters[1]
  end

  def test_ordered_letters_for_same_frequency_letters
    letters = LetterCounter.new(['hello']).counting_ordered_letters.to_a
    assert_equal 'l', letters[0]
    assert_equal 'e', letters[1]
    assert_equal 'o', letters[2]
    assert_equal 'h', letters[3]
  end

  def test_ordered_letters_for_two_words
  	letters = LetterCounter.new(['hello', 'world']).counting_ordered_letters
  	assert_equal 'l', letters.next
  	assert_equal 'o', letters.next
  	assert ['h', 'd', 'e', 'r', 'w'].include? letters.next
  	assert ['h', 'd', 'e', 'r', 'w'].include? letters.next
  	assert ['h', 'd', 'e', 'r', 'w'].include? letters.next
  	assert ['h', 'd', 'e', 'r', 'w'].include? letters.next
  	assert ['h', 'd', 'e', 'r', 'w'].include? letters.next
  end

  def test_count_all_the_words
  	words = CorpusReader.new(100_000).words
  	letters = LetterCounter.new(words).counting_ordered_letters
  	assert_equal 'a', letters.next
  	assert_equal 'e', letters.next
  	assert_equal 'i', letters.next
  	assert_equal 'o', letters.next
  	assert_equal 'c', letters.next
  end
end
