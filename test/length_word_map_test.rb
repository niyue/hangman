require 'corpus/length_word_map'
require 'minitest/autorun'

class LengthWordMapTest < Minitest::Test

  def test_get_length_world_map
  	length_map = LengthWordMap.new(['hello', 'world', 'hi', 'you'])
  					.length_word_map
  	assert_equal 3, length_map.size
    assert_equal ['hello', 'world'], length_map[5]
    assert_equal ['hi'], length_map[2]
    assert_equal ['you'], length_map[3]
  end
end
