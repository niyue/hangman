require 'minitest/autorun'
require 'hangman/client'

class ClientTest < Minitest::Test

  PLAYER_ID = 'niyue.com@gmail.com'

  def setup
    @client = Client.new
    result = @client.start_game(PLAYER_ID)
    assert result
  end

  def test_start_game
    client = Client.new
    result = client.start_game(PLAYER_ID)
    assert result
    assert result['numberOfWordsToGuess']
    assert result['numberOfGuessAllowedForEachWord']
  end

  def test_next_word
    result = @client.next_word
    assert result
  end

  def test_guess_word
    result = @client.next_word
    result = @client.guess_word('e')
    assert result
  end

  def test_get_result
    result = @client.get_result
    assert result['sessionId']
    data = result['data']
    assert data['totalWordCount']
    assert data['correctWordCount']
    assert data['totalWrongGuessCount']
    assert data['score']
  end

  # def test_submit_result
  #   result = @client.submit_result
  #   assert result['playerId']
  #   assert result['sessionId']
  #   assert result['totalWordCount']
  #   assert result['correctWordCount']
  #   assert result['totalWrongGuessCount']
  #   assert result['score']
  #   assert result['datetime']
  # end
end
