require 'hangman/score_submitter'
require 'minitest/autorun'

class ScoreSubmitterTest < Minitest::Test

  def setup
    @submitter = ScoreSubmitter.new('test_score.txt')
  end

  def teardown
    @submitter.reset
  end

  def test_submit_score
    submitted_score = @submitter.submit(1000)
    assert_equal 1000, submitted_score

    submitted_score = @submitter.submit(999)
    assert_nil submitted_score

    submitted_score = @submitter.submit(1001)
    assert_equal 1001, submitted_score
  end
end
