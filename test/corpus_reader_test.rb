require 'corpus/corpus_reader'
require 'minitest/autorun'

class CorpusReadeTest < Minitest::Test

  def test_loading_the_corpus_should_succeeed
  	reader = CorpusReader.new
  	words = reader.words
  	assert_equal 10000, words.size
  end

  def test_loading_the_corpus_with_limit_should_succeeed
    words = CorpusReader.new(100).words
    assert_equal 100, words.size
  end

  def test_loading_all_the_corpus_with_special_limit_should_succeeed
    words = CorpusReader.new(-1).words
    assert words.size > 200_000
  end
end
