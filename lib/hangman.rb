require 'hangman/version.rb'
require 'hangman/play'
require 'hangman/logger'
require 'corpus/extended_corpus_manager'

# Add requires for other files you add to your project here, so
# you just need to require this one file in your bin file
