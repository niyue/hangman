require 'semantic_logger'

color_map = SemanticLogger::Formatters::Color::ColorMap.new
formatter = Proc.new do |log|
  level = "#{log.level.upcase}".ljust(5)
  "#{log.time} #{color_map[log.level]}#{level}#{color_map.clear} #{log.message.ljust(25)} #{log.payload}"
end

SemanticLogger.default_level = ENV['HANGMAN_DEV'].nil? ? :info : :debug
SemanticLogger.add_appender(io: STDOUT, formatter: formatter )
SemanticLogger.add_appender(file_name: File.join('log', "#{Process.pid}.log"), formatter: formatter)

def get_logger(name)
  SemanticLogger[name]
end
