require 'hangman/client'
require 'hangman/word_frequency_gaming_policy'
require 'hangman/fixed_order_gaming_policy'
require 'hangman/logger'
require 'corpus/extended_corpus_manager'
require 'set'
require 'hangman/score_submitter'

@logger = get_logger('hangman.play')

def play(player_id, vocabulary_size_limit)
  client = Client.new

  number_of_words_to_guess = start_game(client, player_id)

  # other available policies: FixedOrderGamingPolicy
  policy = choose_gaming_policy(WordFrequencyGamingPolicy, vocabulary_size_limit)

  @extended_corpus_manager = ExtendedCorpusManager.new
  @score_submitter = ScoreSubmitter.new('hangman_score.txt')
  @correct_word_count = 0

  for i in 1..number_of_words_to_guess
    guessed_word = guess_a_new_word(client, policy, i)

    show_game_progress(guessed_word, i, number_of_words_to_guess)
  end
  score = show_game_play_result(client)
  submit_game_play_result(score, client)
end

private
def start_game(client, player_id)
  game_info = client.start_game(player_id)
  number_of_words_to_guess = game_info['numberOfWordsToGuess']
  @logger.info('game_is_started', 
                number_of_words_to_guess: number_of_words_to_guess)
  number_of_words_to_guess
end

def choose_gaming_policy(policy_class, limit)
  policy_class.new(limit: limit, use_extended_words: true)
end

def guess_a_new_word(client, policy, i)
  word_pattern = client.next_word
  @logger.info('guessing_a_new_word', 
                pattern_length: word_pattern.size,
                ith: i)

  guessed_word = guess_word(client, policy, word_pattern)

  unless word_uncertain?(guessed_word)
    learn_a_new_word(guessed_word)
  end
  guessed_word
end

def guess_word(client, policy, word_pattern)
  guessed_letters = Set.new
  wrongly_guessed_letters = Set.new
  wrong_guess_count = 0
  while word_uncertain?(word_pattern)
    new_pattern, wrong_guess_count = find_next_pattern(client, word_pattern, 
                                                      guessed_letters, wrongly_guessed_letters,
                                                      policy, wrong_guess_count)

    if new_pattern.nil?
      @logger.info('unable_to_guess_word', 
                    word: word_pattern,
                    guess_count: guessed_letters.size)
      return word_pattern
    end

    word_pattern = new_pattern
  end
  @logger.info('guess_correct_word', 
                word: word_pattern, 
                guess_count: guessed_letters.size,
                wrong_guess_count: wrong_guess_count)
  word_pattern
end

def word_uncertain?(word_pattern)
  word_pattern.include? '*'
end

def find_next_pattern(client, word_pattern, guessed_letters, 
                      wrongly_guessed_letters, policy, wrong_guess_count)
  letters = policy.run(word_pattern, wrongly_guessed_letters)
  new_pattern = word_pattern
  while new_pattern.eql?(word_pattern)
    guess_letter = letters.next.upcase
    while letter_guessed?(guessed_letters, guess_letter)
      guess_letter = letters.next.upcase
    end
    new_pattern = client.guess_word(guess_letter)
    guessed_letters.add(guess_letter)

    guess_result = guess_result(word_pattern, new_pattern)
    @logger.info("#{guess_letter} #{guess_result}")
    unless guess_correctly?(word_pattern, new_pattern)
      wrong_guess_count += 1
      wrongly_guessed_letters.add(guess_letter)
    end
  end
  return new_pattern, wrong_guess_count
end

def letter_guessed?(guessed_letters, guess_letter)
  guessed_letters.include? guess_letter
end

def guess_correctly?(word_pattern, new_pattern)
  not (new_pattern.nil? or new_pattern.eql?(word_pattern))
end

def guess_result(word_pattern, new_pattern)
  guess_correctly?(word_pattern, new_pattern) ? '👻' : '💔'
end  

def show_game_progress(guessed_word, i, number_of_words_to_guess)
  if not word_uncertain?(guessed_word)
      @correct_word_count += 1
    end

    estimated_final_correct_words = @correct_word_count * number_of_words_to_guess / i
    @logger.info('game_progress', 
                  success_rate: "#{@correct_word_count}/#{i}",
                  estimated_final_correct_words: "#{estimated_final_correct_words}",
                  latest_word_result: guessed_word)
end

def learn_a_new_word(word)
  @extended_corpus_manager.add(word.downcase)
end

def show_game_play_result(client)
  result = client.get_result
  @logger.info('game_finished', result: result)
  result['data']['score']
end

def submit_game_play_result(score, client)
  submitted_score = @score_submitter.submit(score, client)
  if submitted_score.nil?
    @logger.info('game_score_is_skipped_for_submission', score: score)
  else
    @logger.info('game_score_is_submitted', score: submitted_score)
  end
end

