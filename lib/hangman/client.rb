require 'rest-client'
require 'hangman/logger'
require 'json'

class Client
  BASE_URL = 'https://strikingly-hangman.herokuapp.com/game/on'
  HEADERS = { 'content_type': 'json', 'accept': 'json' }

  def initialize
    @endpoint = RestClient::Resource.new(BASE_URL, timeout: 120)
    @session_id = nil
    @logger = SemanticLogger['hangman.client']
  end

  def start_game(player_id)
    result = send({'action': 'startGame', 'playerId': player_id})
    @session_id = result['sessionId']
    get_data(result)
  end

  def next_word
    get_data(send({'action': 'nextWord'}))['word']
  end

  def guess_word(letter)
    begin
      get_data(send({'action': 'guessWord', 'guess': letter.upcase}))['word']
    rescue RestClient::UnprocessableEntity
      nil
    end
  end

  def get_result
    send({'action': 'getResult'})
  end

  def submit_result
    @logger.info('submit_result_request')
    get_data(send({'action': 'submitResult'}))
  end

  private 
  def send(request)
    for i in 1..3
      if i > 1
        @logger.info('retry_request', request: request, times: i)
      end
      begin
        resp = send_without_retry(request)
        if resp.code == 200
          @logger.debug('request_succeeded', request: request, response: resp.body)
          break
        elsif resp.code == 400
          @logger.error('bad_request', request: request, response: resp.body)
          break
        elsif resp.code == 500
          @logger.error('server_error', request: request, response: resp.body)
        end
      rescue RestClient::Exceptions::Timeout
        @logger.warn('request_timeout', request: request)
      end 
    end

    JSON.parse(resp.body)
  end

  def send_without_retry(request)
    unless @session_id.nil?
      request['sessionId'] = @session_id
    end
    @logger.debug('sending_request', request: request)
    @endpoint.post(request.to_json, HEADERS)
  end

  def get_data(result)
    result['data']
  end
end
