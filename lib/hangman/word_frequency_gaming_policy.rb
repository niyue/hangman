require 'corpus/pattern_matcher'
require 'corpus/corpus_reader'
require 'corpus/letter_counter'
require 'corpus/extended_corpus_manager'
require 'set'
require 'hangman/logger'

class WordFrequencyGamingPolicy

  def initialize(words: [], limit: -1, use_extended_words: false)
    @logger = get_logger('hangmang.wfgp')
    vocabulary = words.empty? ? CorpusReader.new(limit).words : words
    if use_extended_words
      extended_words = ExtendedCorpusManager.new.words
      @logger.info('extended_words_are_loaded', extended_words: extended_words.size)
      vocabulary = extended_words.merge(vocabulary)
    end
    @logger.info('vocabulary_is_loaded', vocabulary_size: vocabulary.size)
    @matcher = PatternMatcher.new(vocabulary)
  end

  def run(word_pattern, not_contained_letters = Set.new)
    word_pattern = word_pattern.downcase
    matched_words = @matcher.matched_words(word_pattern, not_contained_letters)
    letter_enum = LetterCounter.new(matched_words).counting_ordered_letters
    letters_with_pattern_excluded(letter_enum, word_pattern)
  end

  private
  def letters_with_pattern_excluded(letter_enum, pattern)
    Enumerator.new do |y|
      begin
        while true
          next_letter = letter_enum.next
          while pattern.include? next_letter
            next_letter = letter_enum.next
          end
          y.yield next_letter
        end
      rescue StopIteration
        # the underlying letter_enum reaches its end, so we don't yield any more
      end
    end
  end
end
