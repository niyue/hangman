require 'fileutils'

class ScoreSubmitter
  def initialize(score_file)
    @score_file = File.join(File.dirname(__FILE__), score_file)
    FileUtils.touch(@score_file)
  end

  def submit(score, client = nil)
    submitted_score = nil
    File.open(@score_file, 'r+') do |f|
      f.flock(File::LOCK_EX)
      current_score = f.read
      current_score = current_score.nil? ? 0 : current_score.to_i
      if score > current_score
        f.rewind
        f << score
        submitted_score = score
        unless client.nil?
          client.submit_result
        end
      end
    end
    submitted_score
  end

  def reset
    FileUtils.remove(@score_file)
  end
end
