class FixedOrderGamingPolicy
  def initialize(words: [], limit: -1, use_extended_words: false)
  end
  
  # simply return a fixed order of letters 
  # according to the letter frequence of all English words
  # https://en.wikipedia.org/wiki/Letter_frequency
  def run(word_pattern, not_contained_letters)
    ['e', 't', 'a', 'o', 'i', 'n', 's', 'h', 'r', 'd', 'l', 'c', 'u', 
     'm', 'w', 'f', 'g', 'y', 'p', 'b', 'v', 'k', 'j', 'x', 'q', 'z'].to_enum
  end
end
