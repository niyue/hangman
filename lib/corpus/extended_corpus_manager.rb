require 'hangman/logger'
require 'fileutils'

class ExtendedCorpusManager
  def initialize()
    @logger = get_logger('hangman.extended_corpus_manager')
    @words_file = File.join(File.dirname(__FILE__), 
                            "extended_words.txt.#{Process.pid}")
  end

  def add(word)
    open(@words_file, 'a') do |f|
      f.puts word
    end
    @logger.info('vocabulary_extended', new_word: word)
  end

  def words
    word_set = Set.new
    extended_words_files = Dir[File.join(File.dirname(__FILE__), 'extended_words.txt.*')]
    extended_words_files.each do |f|
      File.foreach(f).with_index do |line, line_num|
        word_set.add(line)
      end  
    end
    word_set
  end

  # Clean all the `extended_words.txt.pid` files, they are all words collected from server
  # All the words in these fileds will be merged into `extended_words.txt.00000` file
  def tidyup
    all_words = words
    merged_word_file = File.join(File.dirname(__FILE__), 'extended_words.txt.00000')
    File.open(merged_word_file, 'w') do |f|
      all_words.each { |w| f << w }
    end
    @logger.info('extended_words_files_are_merged', words_size: all_words.size)
    extended_words_file = Dir[File.join(File.dirname(__FILE__), 'extended_words.txt.*')].select {|f| not f.include? '00000'}
    extended_words_file.each do |f|
      FileUtils.remove(f)
    end
    @logger.info('extended_words_files_are_removed', words_file: extended_words_file.size)
  end
end
