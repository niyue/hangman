class CorpusReader
	def initialize(limit = 10_000)
    words_file = File.join(File.dirname(__FILE__), 'words_alpha.txt')
    @words = collect_words(words_file, limit)
	end

  def words
    @words
  end

  private
  def collect_words(words_file, limit)
    words = []
    File.foreach(words_file).with_index do |line, line_num|
      if line_num < limit or limit == -1
        word = get_word(line)
        words << word
      else
        break
      end
    end
    words
  end

  def get_word(line)
    line.split(' ').first
  end
end
