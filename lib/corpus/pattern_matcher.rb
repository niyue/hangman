require 'corpus/length_word_map'
require 'hangman/logger'

class PatternMatcher
  def initialize(words)
    @words = words
    @length_word_map = LengthWordMap.new(words).length_word_map
    @logger = get_logger('hangman.patternmatcher')
  end

  def matched_words(pattern, not_contained_letters = Set.new)
    matched_words = words_matching_pattern(pattern, not_contained_letters)
    @logger.debug('matching_words_with_pattern', 
                pattern: pattern, 
                incorrect_letters: not_contained_letters.to_a,
                matched_words: matched_words.size)
    matched_words
  end

  private
  def words_matching_pattern(pattern, not_contained_letters)
    words_with_length = @length_word_map[pattern.length] || []

    regular_exp = regular_expression(pattern)
    words_with_length.select do |word| 
      word[/#{regular_exp}/] and not has_letter?(word, not_contained_letters)
    end
  end

  def regular_expression(pattern)
    pattern.gsub '*', '[a-z]'
  end

  def has_letter?(word, letter_set)
    word.chars.any? { |c| letter_set.include? c }
  end
end
