class LengthWordMap
  def initialize(words=[])
  	@length_word_map = words.reduce({}) do |map, word|
  		map[word.length] ||= []
      map[word.length] << word
      map
  	end
  end

  def length_word_map
    @length_word_map
  end
end
