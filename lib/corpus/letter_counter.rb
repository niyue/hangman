require 'pqueue'

# https://en.wikipedia.org/wiki/Letter_frequency
LETTER_FREQUENCY = Hash[
  ['e', 't', 'a', 'o', 'i', 'n', 's', 'h', 'r', 'd', 'l', 'c', 'u', 
   'm', 'w', 'f', 'g', 'y', 'p', 'b', 'v', 'k', 'j', 'x', 'q', 'z']
   .map.with_index.to_a
]
class LetterCounter
  def initialize(words)
    @counter = count_letters(words)
  end

  def count
    @counter
  end

  def counting_ordered_letters
    chars_with_count = @counter.map.with_index do |count, index| 
      [(index + 'a'.ord).chr, count]
    end
    queue = PQueue.new(chars_with_count) { |a, b| compare_two_letters(a, b) }
    ordered_letters_enum = Enumerator.new do |y|
      while not queue.empty?
        y.yield queue.pop()[0]
      end
    end
    return ordered_letters_enum
  end

  private
  def count_letters(words)
    a = 'a'.ord
    counter = Array.new(26, 0)
    words.map(&:chars).each do |word_chars|
      word_chars.each do |char|
        counter[char.ord - a] += 1
      end
    end
    return counter
  end

  # if the letter a appears more than letter b in the matching words
  # or if letter a appears the same as letter b, 
  # we fallback to use a fixed LETTER_FREQUENCY for all english letters
  def compare_two_letters(a, b)
    a[1] > b[1] or 
    (a[1] == b[1] and LETTER_FREQUENCY[a[0]] < LETTER_FREQUENCY[b[0]])
  end
end
